# Symfony Startpoint

A symfony boilerplate.

## Features

- [Safe PHP 1.0](https://github.com/thecodingmachine/safe)
- [Twig 3.0](https://twig.symfony.com/)
- [PHPUnit 8.3](https://phpunit.readthedocs.io/en/8.3/)
- [Mockery 1.3](http://docs.mockery.io/en/stable/)
- [phan 2.4](https://github.com/phan/phan/wiki#using-phan)
- [phpstan 0.12](https://github.com/phpstan/phpstan)
- [php-cs-fixer 2.16](https://cs.symfony.com/)

## Easy scripts

All following scripts can be run like:

    composer run-script <script-name>

- `static-analyse`: Runs `phpstan analyse` and `phan`.
- `fix-style`: Runs `php-cs-fixer fix`.
- `test`: Runs `php bin/phpunit` and generates coverage reports in 3 formats
  located in `docs/coverage` directory.
- `full-test`: Runs all three above respectively.
- `docs`: Generates documentations using `doxygen` in `docs/html` and
  `docs/latex` directories.
- `safe-docs`: Runs `full-test` and `docs` respectively.
