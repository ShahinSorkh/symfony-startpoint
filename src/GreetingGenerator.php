<?php

namespace App;

use Psr\Log\LoggerInterface;

class GreetingGenerator
{
    /** @var \Psr\Log\LoggerInterface */
    private $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public function getRandomGreeting(): string
    {
        $greetings = ['hey', 'yo', 'aloha'];
        $greeting = $greetings[array_rand($greetings)];

        $this->logger->debug("using greeting '{$greeting}'");

        return ucfirst($greeting);
    }

    public function getRandomName(): string
    {
        $names = ['world', 'shahin', 'narges', 'fucker'];
        $name = $names[array_rand($names)];

        $this->logger->debug("using name '{$name}'");

        return ucfirst($name);
    }
}
