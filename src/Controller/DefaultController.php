<?php

namespace App\Controller;

use App\GreetingGenerator;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends AbstractController
{
    public function index(LoggerInterface $logger, GreetingGenerator $gen): Response
    {
        $logger->info('THIS IS FUCKING INDEX!!!!');

        $name = $gen->getRandomName();

        return $this->render('default/index.html.twig', compact('name'));
    }

    public function hello(string $name): Response
    {
        return $this->render('default/index.html.twig', compact('name'));
    }
}
