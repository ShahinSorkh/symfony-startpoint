<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class DefaultControllerTest extends WebTestCase
{
    /** @test */
    public function index_works(): void
    {
        $client = static::createClient();
        $client->request('GET', '/');

        $this->assertEquals(
            Response::HTTP_OK,
            $client->getResponse()->getStatusCode()
        );
    }

    /** @test */
    public function hello_works(): void
    {
        $client = static::createClient();
        $client->request('GET', '/hello/tester');

        $this->assertEquals(
            Response::HTTP_OK,
            $client->getResponse()->getStatusCode()
        );

        $this->assertStringContainsStringIgnoringCase(
            'tester!',
            $client->getResponse()->getContent() ?: ''
        );
    }
}
