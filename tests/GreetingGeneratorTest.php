<?php

namespace App\Tests;

use App\GreetingGenerator;
use Mockery;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;

class GreetingGeneratorTest extends TestCase
{
    /** @test */
    public function generates_random_greeting(): void
    {
        $gen = $this->createGreetingGenerator();

        $this->assertTrue(
            in_array($gen->getRandomGreeting(), ['Hey', 'Yo', 'Aloha'])
        );
    }

    /** @test */
    public function generates_random_name(): void
    {
        $gen = $this->createGreetingGenerator();

        $this->assertTrue(
            in_array($gen->getRandomName(), ['World', 'Shahin', 'Narges', 'Fucker'])
        );
    }

    private function createGreetingGenerator(): GreetingGenerator
    {
        $logger = Mockery::mock(LoggerInterface::class);
        $logger->shouldIgnoreMissing();

        return new GreetingGenerator($logger);
    }
}
