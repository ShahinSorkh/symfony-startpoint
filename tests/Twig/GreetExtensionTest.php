<?php

namespace App\Tests\Twig;

use App\GreetingGenerator;
use App\Twig\GreetExtension;
use Mockery;
use PHPUnit\Framework\TestCase;
use Twig\TwigFilter;

class GreetExtensionTest extends TestCase
{
    /** @test */
    public function returns_corrects_filters(): void
    {
        $gen = Mockery::mock(GreetingGenerator::class);
        $ext = new GreetExtension($gen);

        $filters = $ext->getFilters();

        $this->assertInstanceOf(TwigFilter::class, $filters[0]);
    }

    /** @test */
    public function returns_random_greeting(): void
    {
        $gen = Mockery::mock(GreetingGenerator::class);
        $gen->shouldReceive('getRandomGreeting')->andReturn('greet');
        $ext = new GreetExtension($gen);

        $this->assertEquals(
            'greet tester!',
            $ext->greetUser('tester')
        );
    }
}
